package com.example.collatzconjecture

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text
import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    private lateinit var startingNumberInput: EditText
    private lateinit var output: TextView
    private lateinit var sharedPref: SharedPreferences
    private lateinit var highestNumTextView: TextView
    private var highestNum = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return
        highestNum = sharedPref.getInt("highestNum", 0)

        highestNumTextView = findViewById(R.id.highest)
        highestNumTextView.text = getString(R.string.highest_num, highestNum)

        startingNumberInput = findViewById(R.id.startingNumberInput)
        output = findViewById(R.id.output)

        findViewById<Button>(R.id.calculate).setOnClickListener { display3n1() }
    }

    override fun onStop() {
        super.onStop()

        // save highestNum to storage
        with(sharedPref.edit()) {
            putInt("highestNum", highestNum)
            apply()
        }
    }

    private fun display3n1() {
        val numbers: ArrayList<Int> = ArrayList()

        // num initialization
        var num = getStartingNum()
        if (num == -1)
            return

        updateHighestNum(num, numbers)

        // get numbers until 1 is calculated
        while (true) {
            val calculation = calculate3n1(num)
            numbers.add(calculation)

            // stop when 1 is found
            if (calculation == 1)
                break

            num = calculation
        }

        output.text = numbers.joinToString()
    }

    private fun getStartingNum(): Int {
        val num: Int
        try {
            // input validation
            num = startingNumberInput.text.toString().toInt()
            if (num <= 0)
                throw java.lang.NumberFormatException()
        } catch (e: NumberFormatException) {
            Toast.makeText(applicationContext, getString(R.string.invalid), Toast.LENGTH_SHORT)
                .show()
            return -1
        }
        return num
    }

    private fun updateHighestNum(num: Int, numbers: ArrayList<Int>) {
        if (num > highestNum) {
            for (n in highestNum..num) {
                val calculation = calculate3n1(highestNum + n)
                // check if the calculated number has been calculated before (VIOLATES the conjecture)
                if (numbers.contains(calculation)) {
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.violates_conjecture),
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
            }
            // set highestNum
            highestNum = num
            highestNumTextView.text = getString(R.string.highest_num, highestNum)
        }
    }

    private fun calculate3n1(num: Int): Int {
        // ternary-like return
        return if (num % 2 == 0)
            num / 2
        else
            3 * num + 1
    }
}